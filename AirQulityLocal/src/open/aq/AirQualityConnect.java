package open.aq;

import java.io.IOException;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;


public class AirQualityConnect {
    protected Connection connection;
    private List<dataPOJO> datalist;

    public AirQualityConnect() throws SQLException, ClassNotFoundException{
        this.getConnection();
    }

    public static void main( String argv[])throws IOException, Exception{

        System.out.println("Getting Austrialia Data");
        AirQualityConnect aqdb =  new AirQualityConnect();
        System.out.println("Austrialia data from postgress : ");
        aqdb.getAustrialia("*");
    }

    public void getConnection() throws SQLException, ClassNotFoundException {
        try {

            System.out.println("Connecting to database");
            Class.forName("org.postgresql.Driver");
            System.out.println("Driver loaded");
            String jdbcUrl = "jdbc:postgresql://openaq-db.cytnirfj3dtl.us-east-1.rds.amazonaws.com:5432/openaq-production?user=openaq&password=openaq12345";
            connection = DriverManager.getConnection(jdbcUrl);
            System.out.println("Connected with database :"+ connection);
        }catch(ClassNotFoundException e){
            System.out.println("class Exception :"+e.getMessage());
        }catch(SQLException ex){
            System.out.println("SQL Exception :"+ex.getMessage());
        }
    }

    public void getAustrialia(String sent)throws IOException , Exception{
        ResultSet rset=null;
        PreparedStatement preparedSment=null;
        String sql="";
        String _id, location, value, unit, parameter, country, city;
        datalist = new LinkedList<dataPOJO>();

        try{
            if(sent.equals("*")) {
                sql = "SELECT * FROM measurements WHERE country LIKE 'AU' ORDER BY _id ASC LIMIT 100";
            }else{
                sql="SELECT * FROM measurements WHERE country LIKE 'AU' AND parameter LIKE '" + sent + "' ORDER BY _id ASC LIMIT 100";
            }

            preparedSment = connection.prepareStatement(sql);
            rset = preparedSment.executeQuery();

            while(rset.next()){
                _id = rset.getString(1);
                location = rset.getString(2);
                value = rset.getString(3);
                unit = rset.getString(4);
                parameter = rset.getString(5);
                country = rset.getString(6);
                city = rset.getString(7);

                dataPOJO data = new dataPOJO(_id, location, value, unit, parameter, country, city);
                datalist.add(data);
            }
            rset.close();
            preparedSment.close();
        }
        catch(SQLException ex){
            System.out.println("SQL Exception :"+ex.getMessage());
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try {
                if (rset != null) {
                    rset.close();
                }
                if (preparedSment != null) {
                    preparedSment.close();
                }

            } catch (Exception ex1) {
            }
        }
    }

/*
    public void getSpecificData(String para)throws IOException , Exception{
        ResultSet rset=null;
        PreparedStatement preparedSment=null;
        String sql="";
        String _id, location, value, unit, parameter, country, city;
        datalist = new LinkedList<dataPOJO>();

        try{
            sql="SELECT * FROM measurements WHERE country LIKE 'AU' AND parameter LIKE '" + para + "' ORDER BY _id ASC LIMIT 100";

            preparedSment = connection.prepareStatement(sql);
            rset = preparedSment.executeQuery();

            while(rset.next()){
                _id = rset.getString(1);
                location = rset.getString(2);
                value = rset.getString(3);
                unit = rset.getString(4);
                parameter = rset.getString(5);
                country = rset.getString(6);
                city = rset.getString(7);

                dataPOJO data = new dataPOJO(_id, location, value, unit, parameter, country, city);
                datalist.add(data);
            }
            rset.close();
            preparedSment.close();
        }
        catch(SQLException ex){
            System.out.println("SQL Exception :"+ex.getMessage());
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }finally{
            try {
                if (rset != null) {
                    rset.close();
                }
                if (preparedSment != null) {
                    preparedSment.close();
                }

            } catch (Exception ex1) {
            }
        }
    }
*/

    public List<dataPOJO> getDatalist(){
        return datalist;
    }

}
