package open.aq;

import java.io.IOException;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Region;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.ReturnValue;

public class DynamoDBNewsLetter {

    static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
//    static AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withRegion(Regions.US_EAST_1).build();

    static DynamoDB dynamoDB = new DynamoDB(client);

    private String tableName = "AirQualityReg";


    protected Connection connection;

    public DynamoDBNewsLetter() throws SQLException, ClassNotFoundException{
        this.getConnection();
    }

    public void getConnection() throws SQLException, ClassNotFoundException {
//        try {

            System.out.println("Connecting to dynamodb");
//            Class.forName("cdata.jdbc.dynamodb.DynamoDBDriver");
//            String jdbcUrl = "jdbc:dynamodb:Access Key=AKIAIAXYIQTRYGHYR63Q;Secret Key=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;Domain=amazonaws.com;Region=northernvirginia";
//            connection = DriverManager.getConnection(jdbcUrl);
            System.out.println("Connected with dynamodb :"+ connection);
//        }catch(SQLException ex){
//            System.out.println("SQL Exception :"+ex.getMessage());
//        }
    }

    public static void main(String[] args) throws IOException, SQLException, ClassNotFoundException {
        DynamoDBNewsLetter dbNewsLetter = new DynamoDBNewsLetter();

        dbNewsLetter.pullResults("Chris", "Sanchez", "csanchr@gmail.com","Belize", "25");
        // boolean ret = stat.execute("SELECT * FROM Account");
        //ResultSet rs=stat.executeQuery();
        /*while(rs.next()){
            for(int i=1;i<=rs.getMetaData().getColumnCount();i++)
            {
                System.out.println(rs.getMetaData().getColumnName(i) +"="+rs.getString(i));
            }
        }*/

    }

    public void pullResults(String fname, String lname, String email, String country, String age) throws SQLException, ClassNotFoundException{
        DynamoDBNewsLetter dbNewsLetter = new DynamoDBNewsLetter();

        dbNewsLetter.insertItems(fname, lname, email, country, age);

    }
    private void insertItems(String fname, String lname, String email, String country, String age) throws SQLException {
        /*PreparedStatement stat = null;

        String sql = "INSERT INTO AirQualityReg (ID, FNAME, LNAME, EMAIL, COUNTRY, AGE) VALUES (1, ?, ?, ?, ?, ?)";

        System.out.println(sql);
        System.out.println(fname + " " + lname + " " + email + " " +  country + " " + age);

        stat = connection.prepareStatement(sql);
        stat.setString(1, fname);
        stat.setString(2, lname);
        stat.setString(3, email);
        stat.setString(4, country);
        stat.setInt(5, Integer.parseInt(age));

        stat.executeUpdate();*/
try {
    Table table = dynamoDB.getTable(tableName);

    SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
    System.out.println("Adding data to " + tableName);
    String Id = dateFormatter.format(new Date());

// Build the item
    Item item = new Item()
            .withPrimaryKey("EMAIL", email)
            .withString("FNAME", fname)
            .withString("LNAME", lname)
            .withString("ID", "125")
            .withString("COUNTRY", country)
            .withNumber("AGE", Integer.parseInt(age));

    item.withString("RegDateTime", (dateFormatter.format(new Date())));
    table.putItem(item);

    System.out.println("Subscription is successful");
}catch (Exception e){
    e.printStackTrace();
}

    }

}
