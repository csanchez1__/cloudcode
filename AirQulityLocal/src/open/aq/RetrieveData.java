package open.aq;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RetrieveData extends HttpServlet {

    public RetrieveData(){

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DynamoDBNewsLetter dbNewsLetter = null;
        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String email = request.getParameter("email");
        String country = request.getParameter("country");
        String age = request.getParameter("age");

        System.out.println(fname + " " + lname + " " +  email + " " + country + " " + age);

        try {
            dbNewsLetter = new DynamoDBNewsLetter();
            dbNewsLetter.pullResults(fname, lname, email, country, age);
            response.sendRedirect("index.jsp");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doGet (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
        doPost(req, resp);
    }

}
