<%@page import="java.util.ArrayList, open.aq.*,
java.text.DecimalFormat,
java.sql.SQLException"%>
<%@ page import="java.util.List" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <%--<meta name="robots" content="index, follow" />--%>
    <meta name="description" content="Air Quality Display information from OpenAQ" />
    <%--<meta name="Keywords" content="Customer Care, CRM, Telecom Billing, Utility Billing, Interconnect Billing, Wholesale Billing, Voice Trading, Data Billing, Wi-max Billing, GSM Billing, Converged Billing, IGW Billing, ICX Billing, Converged Rating, Discount, Wavers, Invoice , Collection, Payment Processing, Dispute, Adjustments, Reports Generation, Billing Types, Billing Standards, Billing Terminology, Convergence billing, Retail Billing, Enterprise Billing, ERP, Textile ERP, Trading ERP, Security ERP, Import Export ERP, Livestock, Dairy, Livestock ERP, Livestock Portal, Dairy Portal" />--%>
    <meta name="author" content="Tech Army" />
    <meta name="revisit-after" content="1 day" />
    <title>Tech Army Air Quality</title>
    <link href="style/style.css" rel="stylesheet" type="text/css" />
    <link href="style/csshorizontalmenu.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="images/terminus_favicon.ico" />
    <script type="text/javascript" src="css/csshorizontalmenu.js">

        /***********************************************

         * CSS Horizontal List Menu- by JavaScript Kit (www.javascriptkit.com)
         * Menu interface credits: http://www.dynamicdrive.com/style/csslibrary/item/glossy-vertical-menu/
         * This notice must stay intact for usage
         * Visit JavaScript Kit at http://www.javascriptkit.com/ for this script and 100s more

         ***********************************************/

    </script>
    <!-- Start Slider HEAD section -->
    <link rel="stylesheet" type="text/css" href="css/slider_style.css" />
    <script type="text/javascript" src="css/jquery.min.js"></script>
    <script type="text/javascript" src="css/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="css/script.js"></script>
    <script type="text/javascript" src="css/jquery-1.7.1.min.js"></script>
    <script type="text/javascript" src="css/slider_file.js"></script>
    <!-- End Slider HEAD section -->
    <style type="text/css">
        #tb-customer {
            position:absolute;
            width:240px;
            height:164px;
            z-index:99999;
            left: 62px;
            top: 522px;
        }

        .tblMain{
            width: 100%;
            /* border-collapse: collapse; */
        }
        .tblGrid{
            width: 99%;
            border-color: #fff;
            border-collapse: collapse;
            layout-grid: 1px solid red;
            text-align: left;
        }

        .dispFieldTitle{
            color:#171616;
            font-family:  Arial, Verdana, Geneva, Trebuchet MS,  Helvetica, sans-serif;
            font-size: 12px;
            text-align: right;
        }

        .normalText{
            font-size: 12px;
            font-family:  Arial, Verdana, Geneva, Trebuchet MS,  Helvetica, sans-serif;
            color:#171616;
            padding-left:5px;
        }

        .txtInput{
            height: 22px;
            font-size: 11px;
            background:#f8f8f8;
            border:1px solid #6FB36A;
            padding-left: 2px;
        }

        .btnInput{

            font-size: 11px;
            width:100px;
            line-height:25px;
            height:25px;
            border:none;
            background:#038559;
            color:#FFF;
            text-align:center;
            cursor:pointer;

        }


    </style>
    <script type="text/javascript">
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
    </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-49262557-1', 'terminustech.com');
        ga('send', 'pageview');

    </script>

    <script language='JavaScript'>
        function getspendingInfo(f) {
            f.view.value = 1;
            f.insertRecord.value=0;
            f.submit();
        }
        function insertSubscriber(f) {
            f.insertRecord.value=1;
            f.view.value = 0;
            f.submit();
        }
    </script>

</head>

<body>

<form action="retrieve" method="post">
    <label>First Name:</label>
    <input type="text" name="fname">
    <label>Last Name:</label>
    <input type="text" name="lname">
    <label>Country:</label>
    <input type="text" name="country">
    <label>Age:</label>
    <input type="text" name="age">

    <input type="submit" value="Submit">
</form>

<form action="DisplayQuery.jsp">
    <label>Parameter:</label>
    <select name="Parameter">
        <option>no2</option>
        <option>pm25</option>
        <option>so2</option>
        <option>o3</option>
        <option>pm10</option>
        <option>co</option>
    </select>
    <input type="submit" value="Submit">
</form>

<%
AirQualityConnect airQ = new AirQualityConnect();
String paraSelect = paraSelect = request.getParameter("Parameter");
try {
    if (paraSelect.equals(null)) {
        airQ.getAustrialia();
    } else {
        airQ.getSpecificData(paraSelect);
    }
}catch (NullPointerException e){
    airQ.getAustrialia();
}
%>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <thead>
    <td>_id</td>
    <td>location</td>
    <td>value</td>
    <td>unit</td>
    <td>parameter</td>
    <td>country</td>
    <td>city</td>
    </thead>
    <%
        List<dataPOJO> resultList = airQ.getDatalist();
        for(dataPOJO data : resultList){
    %>
    <tr>
        <td><%=data.get_id()%></td>
        <td><%=data.getLocation()%></td>
        <td><%=data.getValue()%></td>
        <td><%=data.getUnit()%></td>
        <td><%=data.getParameter()%></td>
        <td><%=data.getCountry()%></td>
        <td><%=data.getCity()%></td>
    </tr>
    <%
        }
    %>
</table>



<%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td align="center"><table width="990" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr> <td width="1%"><a href="http://www.terminustech.co.nz/index.htm"><img src="images/logo.jpg" style="border:none; "width="180" height="84" /></a></td>
                        <td width="99%" align="right"><table width="95%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="75%"><img src="images/space.gif" width="2" height="55" /></td>
                                <td width="25%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="1%" align="left">
                                    <div class="horizontalcssmenu">
                                        <ul id="cssmenu1">
                                            <li><a href="http://www.terminustech.co.nz/index.htm"><span></span>&nbsp;&nbsp;Home</a></li>
                                            <li><a href="#">Products</a>
                                                <ul>
                                                    <li><a href="http://www.terminustech.co.nz/Customer-Care-Billing.htm">Customer Care & Billing System</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Interconnect-Billing.htm">Interconnect Billing System</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Terminus-CRM.htm">Terminus CRM</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Enterprise-Resource.htm">Enterprise Resource Planning</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Livestock-MIS.htm">Livestock MIS</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/rent-a-car.html">Fleet Manager</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/hotel-reservation.html">Hotel Reservation</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/campus-management.html">Campus Management</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/payroll-system.html">HR / Payroll</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="#">Services</a>
                                                <ul>
                                                    <li><a href="http://www.terminustech.co.nz/Project-Management.htm">Project Management</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Software-Development.htm">Software Development</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/System-Integration.htm">System Integration</a></li>

                                                </ul>
                                            </li>
                                            <li><a href="#">Company</a>
                                                <ul>
                                                    <li><a href="http://www.terminustech.co.nz/Company-Profile.htm">Company Profile</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Company-Vision.htm">Company Vision</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Market-Coverage.htm">Market Coverage & Strategy</a></li>
                                                    <li><a href="http://www.terminustech.co.nz/Management.htm">Management</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="./GetSpendingInfo.jsp"><span></span>&nbsp;&nbsp;Budget Info</a></li>
                                            <li><a href="./GetSubscribers.jsp"><span></span>Subscribers</a></li>

                                        </ul>
                                        <br style="clear: left;" />
                                    </div>
                                </td>
                            </tr>
                        </table></td>
                    </tr>
                </table></td>
            </tr>
            <tr>
                <td><img src="images/space.gif" width="2" height="15" /></td>
            </tr>
            <tr>
                <td><!-- Start Slider -->
                    <div class="banner_box">



                        <div id="container">



                            <!--  Outer wrapper for presentation only, this can be anything you like -->
                            <div id="banner-fade">
                                <!-- start Basic Jquery Slider -->
                                <ul class="bjqs">
                                    <li><img src="images/banner01.png"  width="990"></li>
                                    <li><img src="images/banner06.png"  width="990"></li>
                                    <li><img src="images/banner07.png"  width="990"></li>
                                    <li><img src="images/banner02.png"  width="990"></li>
                                    <li><img src="images/banner03.png"  width="990"></li>
                                    <li><img src="images/banner04.png"  width="990"></li>
                                    <li><img src="images/banner05.png"  width="990"></li>
                                    <li><img src="images/banner08.png"  width="990"></li>
                                    <li><img src="images/car-rental-banner.jpg"  width="990"></li>
                                    <li><img src="images/campus-management.jpg"  width="990"></li>
                                </ul>
                                <!-- end Basic jQuery Slider -->

                            </div>
                            <!-- End outer wrapper -->

                            <script class="secret-source">
                                jQuery(document).ready(function($) {

                                    $('#banner-fade').bjqs({
                                        height      : 332,
                                        width       : 990,
                                        responsive  : true
                                    });

                                });
                            </script>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td><img src="images/space.gif" width="2" height="30" /></td>
            </tr>

&lt;%&ndash;            <%

                String view = request.getParameter("view");
                if (view == null) view="0";

                String insertRecord = request.getParameter("insertRecord");
                if (insertRecord == null) insertRecord="0";


                String SpendingHead1 = request.getParameter("SpendingHead1");
                if (SpendingHead1 == null) SpendingHead1="";


                String SpendingHead2 = request.getParameter("SpendingHead2");
                if (SpendingHead2 == null) SpendingHead2="";


                DecimalFormat df = new DecimalFormat("#,##0.00");
                double SpentValue=0;
                String spentValue = request.getParameter("SpentValue");
                if (spentValue == null || spentValue.length()==0) spentValue="89898.909";

                try{
                    SpentValue = Double.parseDouble(spentValue);
                }catch (Exception ex){
                    SpentValue=0;
                }


                String Message="";
                if (view.equalsIgnoreCase("1")){
                    PGDBManager db = null;
                    try{
                        db = new PGDBManager();
                        SpentValue = db.getSpendingInfo(SpendingHead1, SpendingHead2);
                    } catch (SQLException e) {
                        Message = "There is a Technical Problem";
                    } catch (ClassNotFoundException e) {
                        Message = "Database Connection Error";
                    } finally {
                        try {
                            if (db != null)
                                db.disconnectDatabase();
                        } catch (Exception e) {
                        }
                    }
                }

                if (insertRecord.equalsIgnoreCase("1")){

                    String Name = request.getParameter("SubscriberName");
                    if (Name == null) Name="";

                    String Phone = request.getParameter("Phone");
                    if (Phone == null) Phone="";

                    String Email = request.getParameter("Email");
                    if (Email == null) Email="";

                    String Remarks = request.getParameter("Remarks");
                    if (Remarks == null) Remarks="";

                    String Heads = SpendingHead1;
                    if (SpendingHead2.length() > 0) Heads += ", "+SpendingHead2;


                    DynDBManager db = null;
                    try{
                        db = new DynDBManager();
                        System.out.println("record :"+Name+" --> "+Phone+" --> "+Email+" --> "+Heads+" --> "+SpentValue+" --> "+Remarks);
                        Message = db.InsertSubscriber(Name, Phone, Email, Heads, SpentValue, Remarks);
                        System.out.println("insert Message :"+Message);
                    }catch (SQLException e) {
                        Message = "There is a Technical Problem";
                    } catch (ClassNotFoundException e) {
                        Message = "Database Connection Error";
                    } finally {
                        try {
                            //if (db != null)
                            //db.disconnectDatabase();
                        } catch (Exception e) {
                        }
                    }
                }


                if (Message.length() > 0)
                    System.out.println("Error Message :"+Message);
            %>&ndash;%&gt;

            <tr>
                <td colspan="100%"> <table  width="95%">
                    <tr>
                        <td></td>
                        <td class="normalText" colspan="100%" align="left"><h3><b><font color="green"><%=Message%></font></b></h3></td>
                    </tr>

                    <form name='cardsForm' action='GetSpendingInfo.jsp' method='post'>
                        <tr>
                            <td></td>
                            <td class="normalText" colspan="100%" align="left"><h3><b>Enter Head information to get USA Govt spending over it......... </b></h3></td>
                        </tr>
                        <tr>
                            <td class="dispFieldTitle" width="20%"> Project Name </td>
                            <td width="20%"><input type="text" name="SpendingHead1" class="txtInput"  value="<%=SpendingHead1%>" ></td>
                            <td class="dispFieldTitle" width="20%"> Project Name</td>
                            <td width="20%"><input type="text" name="SpendingHead2" class="txtInput"  value="<%=SpendingHead2%>" ></td>
                            <td  align="right" width="20%">
                                <input type="hidden" name="view" value="1">
                                <%if (view.equalsIgnoreCase("1")){ %>
                                <input name="view" type="button" class="btnInput" onClick='return getspendingInfo(cardsForm)' value="Refresh">
                                <%}else{%>
                                <input name="view" type="button" class="btnInput" onClick='return getspendingInfo(cardsForm)' value="View">
                                <%} %>
                            </td>
                        </tr>
                        <%
                            if (view.equalsIgnoreCase("1")){
                        %>
                        <tr>
                            <td><img src="images/space.gif" width="2" height="15" /></td>
                        </tr>

                        <tr>
                            <td></td>
                            <td colspan="100%" align="left" class="normalText"><b>Total budget spent over above Projects is USD <%=df.format(SpentValue) %></b>
                                <input type="hidden"  name="SpentValue" value="<%=df.format(SpentValue)%>" >
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/space.gif" width="2" height="10" /></td>
                        </tr>

                        <tr>
                            <td class="dispFieldTitle"> Name </td> <td><input type="text"  class="txtInput" name="SubscriberName" value="" ></td>
                            <td class="dispFieldTitle"> Phone </td> <td><input type="text"  class="txtInput" name="Phone" value="" ></td>
                        </tr>
                        <tr>
                            <td class="dispFieldTitle"> Email </td> <td  colspan="100%"><input  class="txtInput" type="text" name="Email" size="60" value="" ></td>
                        </tr>
                        <tr>
                            <td class="dispFieldTitle"> Remarks </td> <td  colspan="100%">
                            <textarea  name="Remarks" cols="40" rows="4"></textarea>
                        </td>
                        </tr>
                        <tr>
                            <td class="dispFieldTitle">  </td> <td  colspan="3">
                        </td>
                            <td align="right" colspan="100%">
                                <input type="hidden" name="insertRecord" value="1">
                                <input name="view" type="button" class="btnInput" onClick='return insertSubscriber(cardsForm)' value="Subscribe">
                            </td>
                        </tr>
                        <%}else{%>
                        <input type="hidden" name="insertRecord" value="0">
                        <%} %>
                    </form>
                </table>
                </td>
            </tr>

            <tr>
                <td><img src="images/space.gif" width="2" height="30" /></td>
            </tr>

            <tr>
                <td><img src="images/space.gif" width="2" height="5" /></td>
            </tr>
        </table></td>
    </tr>

</table>--%>
</body>
</html>
