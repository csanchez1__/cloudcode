<%--
  Created by IntelliJ IDEA.
  User: christopher
  Date: 27/08/2018
  Time: 7:03 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  <title>Welcome to AQ BOt!</title>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <!-- Navbar content -->
        <div class="container">
            <a class="navbar-brand" href="index.jsp">Air Quality Bot</a>
            </div>
          </div>
      </nav>

      
    
  <div id="particles-js"></div>
  <div class="home">
      <div class="dark-overlay landing-inner text-light">
        <div class="container">
          <div class="row">
            <div class="col-md-12 text-center">
              <h1 class="display-3 mb-4">Welcome to Air Quality Bot
              </h1>
              <p class="lead">This website provides you real-time data of Air pollutants for Australia Region!</p>
              <hr />
              <a href="register.jsp" class="btn btn-lg btn-info mr-2">Join</a>
              <a href="data.jsp" class="btn btn-lg btn-light">View Data</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
  
  <script src="http://threejs.org/examples/js/libs/stats.min.js"></script>
  <script src="index.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




</body>

</html>

