<%@page import="java.util.ArrayList, open.aq.*,
java.text.DecimalFormat,
java.sql.SQLException"%>
<%@ page import="java.util.List" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="data.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.src.js"></script>
  <title>Welcome to AQ BOt!</title>
</head>

<body>
    <nav class="navbar navbar-dark bg-dark">
        <!-- Navbar content -->
        <div class="container">
            <a class="navbar-brand" href="index.jsp">Air Quality Bot</a>
            </div>
          </div>
      </nav>



    <form action="data.jsp">
        <label>Parameter:</label>
        <select name="Parameter">
            <option>no2</option>
            <option>pm25</option>
            <option>so2</option>
            <option>o3</option>
            <option>pm10</option>
            <option>co</option>
        </select>
        <input type="submit" value="Submit">
    </form>

    <%
        AirQualityConnect airQ = new AirQualityConnect();
        String paraSelect = paraSelect = request.getParameter("Parameter");
        try {
            if (paraSelect.equals(null)) {
                airQ.getAustrialia("*");
            } else {
                airQ.getAustrialia(paraSelect);
            }
        }catch (NullPointerException e){
            airQ.getAustrialia("*");
        }
    %>

    <div class="container">
    <table width="100%" border="0" cellspacing="0" cellpadding="0" id="dt-table">
        <thead>
        <td>_id</td>
        <td>location</td>
        <td>value</td>
        <td>unit</td>
        <td>parameter</td>
        <td>country</td>
        <td>city</td>
        </thead>
        <%
            List<dataPOJO> resultList = airQ.getDatalist();
            for(dataPOJO data : resultList){
        %>
        <tr>
            <td><%=data.get_id()%></td>
            <td><%=data.getLocation()%></td>
            <td><%=data.getValue()%></td>
            <td><%=data.getUnit()%></td>
            <td><%=data.getParameter()%></td>
            <td><%=data.getCountry()%></td>
            <td><%=data.getCity()%></td>
        </tr>
    <%
            }
        %>
    </table>
        <div id="chart"></div>
    </div>

  <div id="particles-js"></div>




  
  <script src="http://threejs.org/examples/js/libs/stats.min.js"></script>
  <script src="index.js"></script>
    <script src="data.js"></script>
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>




</body>

</html>
